from django.db import models 
from django.forms import ModelForm
from django.db.models import Count
from datetime import datetime
import uuid
from django.contrib.auth.models import User
import json
from django.db.models import Q
from django.db import models
from time import time


class Central_Coordinator(models.Model):
      '''
      This stores the details about which user has which designation related to central server.
      '''
      user = models.ForeignKey(User, db_index=True)
      is_approved = models.BooleanField()
       
      def __str__(self):
	 return str(self.user)

class State(models.Model):

       ''' 
       Model to store states of the country
       '''
       name = models.CharField(max_length=45, unique=True)
     
       def __str__(self):
	 return self.name


class City(models.Model):

       
       ''' 
       Model to store cities and also to which state it belongs
       '''
       name = models.CharField(max_length=45)
       state = models.ForeignKey(State, db_index=True)

       def __str__(self):
	 return self.name

class Accreditation(models.Model):

       '''
       This would store all the accreditation that an institute can have.
       '''
       name = models.CharField( max_length=10)

       def __str__(self):
	 return self.name

       

class Role(models.Model):

      '''
      This would store all the roles that an user can have.
      '''
      name = models.CharField(max_length=30, unique=True)
    
      def __str__(self):
	 return self.name

class Status(models.Model):
      description = models.CharField(max_length=20, unique=True)

      def __str__(self):
	 return self.description
      
class Course(models.Model):
     
      '''
      This would store all the courses created.
      '''
      name = models.CharField(max_length=100, unique=True)

      def __str__(self):
	 return str(self.name)

class Institute_Status(models.Model):

       ''' 
       Model to store status
       '''
       name = models.CharField(max_length=20, unique=True)
     
       def __str__(self):
	 return self.name

class Institute_Registration(models.Model):

     ''' 
     Model to store institute details when an institute registers
     It stores the name , city , state , address, pincode , website.
     status and is_parent are for the use of central coordinator
     After verification , if institute is legit and met the requirement criteria the 
     central coordinator will set status to approved by default it is pending. 
     If institute is a university( Parent of various other institutions)
     then is_parent is set to True by central coordiantor.

     '''


     name = models.CharField(max_length=100, db_index=True, unique=True)
     state = models.ForeignKey(State )
     city = models.ForeignKey(City )
     pincode = models.IntegerField()
     address = models.CharField(max_length=150)
     website = models.CharField(max_length=50)
     is_parent = models.BooleanField()
     status = models.ForeignKey(Institute_Status)
     remarks = models.CharField(max_length=100, null=True)

     def __str__(self):
	 return self.name



class Student_Institute(models.Model):

      '''
      This stores information about the student i.e. when for which period the student is active with an institute.
      A student can belong to multiple institutes at the same time.
      '''
      user = models.ForeignKey(User, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      course = models.ForeignKey(Course, db_index=True)
      active_from = models.DateTimeField(auto_now_add=True, null=True)
      active_upto = models.DateTimeField(auto_now_add=True, null=True)
      status = models.ForeignKey(Institute_Status)

      class Meta:
         unique_together = ('user', 'institute', 'course',)
     
      def __str__(self):
	 return str(self.user)

      @classmethod
      def is_exist(self, user, institute):
          """
          Returns True if the user in Institute 
          Otherwise, returns False.

          `user` is a Django User object.
          institute` is enrolled institiute
          """
          try:
              record = Student_Institute.objects.get(user=user, institute=institute)
              return True
          except self.DoesNotExist:
              return False

      @classmethod
      def is_existcourse(self, user, institute, course): 
          """
          Returns True if the user in Institute course
          Otherwise, returns False.

          `user` is a Django User object.
          institute` is enrolled institiute
          """
          try:
              record = Student_Institute.objects.get(user=user, institute=institute,  course=course)
              return True
          except self.DoesNotExist:
              return False


class Institute_Course(models.Model):
     
      '''
      This stores which insitute is allowed to participate in which courses.
      '''
      course = models.ForeignKey(Course, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      is_approved = models.BooleanField(default=False)

      def _get_institute_name(self):
          #Returns the identity name.
          return str(self.institute.name)
      institute_name = property(_get_institute_name)


      def __str__(self):
	 return str(self.institute)

class Course_Registration(models.Model):

      '''
      This stores the details when a user register with a course under the institute.
      '''

      user = models.ForeignKey(User, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      course = models.ForeignKey(Course, db_index=True)
      status = models.ForeignKey(Institute_Status, db_index=True)
      role = models.ForeignKey(Role, db_index=True)
      is_approved = models.BooleanField()

      def __str__(self):
	 return "{0}, {1}".format(str(self.user) , str(self.institute))

      @classmethod
      def is_exist(self, user, institute, course):
          '''
          Returns True if the user in Student_Document 
          Otherwise, returns False.

          `user` is a Django User object.
          institute` is enrolled institiute
          '''
          try:
              record = Course_Registration.objects.get(user=user, institute=institute, course=course)
              return True
          except self.DoesNotExist:
              return False

      @classmethod
      def inInstitute(self, user, institute):
          '''
      	  Returns True if the user in Institute 
      	  Otherwise, returns False.
      	      
          user is a Django User object.
          institute` is enrolled institiute
          '''
          try:
              record = self.objects.get(user=user, institute=institute)
              return True
          except self.DoesNotExist:
              return False


       

class Hierarchy(models.Model):
      parent_id = models.ForeignKey(Institute_Registration, db_index=True)
      child_id = models.IntegerField()

      def __str__(self):
	 return "{0}, {1}".format(str(self.parent_id) , str(self.child_id))
       

class Person(models.Model):

      '''
      This would store the additional details of the user while registration. Because auth_user and auth_userprofile
      doesn't store birth date and mobile number of the user.
      '''
      user = models.OneToOneField(User, unique=True, db_index=True, related_name='person')
      birth_date = models.DateField(blank=True, null=True)
      mobile = models.BigIntegerField(blank=True, null=True)
      country_code = models.CharField(blank=True, null=True, max_length=3)

      def get_meta(self):
        js_str = self.meta
        if not js_str:
            js_str = dict()
        else:
            js_str = json.loads(self.meta)

        return js_str

      def set_meta(self, js):
        self.meta = json.dumps(js)

      def __str__(self):
	 return str(self.user)


class Institute_Designation(models.Model):

      '''
      This stores the details about which user has which designation related to that institute.
      '''
      user = models.ForeignKey(User, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      role = models.ForeignKey(Role, db_index=True)
      is_approved = models.BooleanField()
       
      def __str__(self):
	 return str(self.institute)


       
class Institute_Accreditation(models.Model):

      '''
      This stores the details which institute have which accreditaitons. 
      '''
      accreditation = models.ForeignKey(Accreditation, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)

      def __str__(self):
	 return str(self.institute)


class Identity(models.Model):

       ''' 
       Model to store identity a person can have.
       '''
       name = models.CharField(max_length=45, unique=True)
     
       def __str__(self):
	 return self.name


class Institute_Identity(models.Model):

       ''' 
       This stores the details which institute is accepting which identities. 
       '''
       identity = models.ForeignKey(Identity, db_index=True)
       institute = models.ForeignKey(Institute_Registration, db_index=True)

       def _get_identity_name(self):
            #Returns the identity name.
            return str(self.identity.name)
       identity_name = property(_get_identity_name)
     
       def __str__(self):
	 #return self.institute
         return str(self.identity.name)


class Admin_Institute(models.Model):
      '''
      This stores information about the institute admin     
      '''
      user = models.ForeignKey(User, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      active_from = models.DateTimeField(auto_now_add=True, null=True)
      active_upto = models.DateTimeField(auto_now_add=True, null=True)
      status = models.ForeignKey(Institute_Status)
     
      class Meta:
         unique_together = ('user', 'institute',)
     
      def __str__(self):
	 return str(self.id)

      @classmethod
      def is_exist(self, user, institute):
          """
          Returns True if the user is Admin Institute 
          Otherwise, returns False.

          `user` is a Django User is registered isActive user.
          institute` is enrolled institiute
          """
          try:
              record = Admin_Institute.objects.get(user=user, institute=institute)
              return True
          except self.DoesNotExist:
              return False



class Faculty_Institute(models.Model):

      '''
      This stores information about the faculty i.e. which faculty belongs to which institute.
      '''
      user = models.ForeignKey(User, db_index=True)
      institute = models.ForeignKey(Institute_Registration, db_index=True)
      status = models.ForeignKey(Institute_Status)
      course = models.ForeignKey(Course, db_index=True,default=0)

      class Meta:
         unique_together = ('user', 'institute','course')

      def __str__(self):
         return str(self.user)

class Student_Identity(models.Model):

       ''' 
       This stores the details which institute is accepting which identities. 
       '''
       user = models.ForeignKey(User, db_index=True)
       institute_identity = models.ForeignKey(Institute_Identity, db_index=True)
       number = models.CharField(max_length=20)
       file_name = models.FileField(upload_to='images',default='settings.MEDIA_ROOT/images/logo.png')
       class Meta:
         unique_together = ('user','institute_identity', 'number',)
       
       def __str__(self):
	 return self.institute_identity


class Student_Document(models.Model):
    ''' 
       This stores the details which institute is accepting which identities. 
       identitiy proof - Document File upload  
    '''
    user = models.ForeignKey(User, db_index=True)
    institute_identity = models.ForeignKey(Institute_Identity, db_index=True)
    proofnumber = models.CharField(max_length=100)
    #docfile = models.FileField(upload_to=file, verbose_name='Document')
    filename = models.CharField(max_length=100)
    
    class Meta:
         unique_together = ('user','institute_identity', 'proofnumber',)

    def save(self, *args, **kwargs):
        #correct = handle_uploaded_file
        super(Student_Document, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id)

    @classmethod
    def is_exist(self, user, institute_identity, proofnumber):
        '''
        Returns True if the user in Student_Document 
        Otherwise, returns False.

        `user` is a Django User object.
        institute` is enrolled institiute
        '''
        try:
            record = Student_Document.objects.get(user=user, institute_identity=institute_identity, proofnumber=proofnumber)
            return True
        except self.DoesNotExist:
            return False


class Institute_Sync(models.Model):

      institute = models.ForeignKey(Institute_Registration, db_index=True)
      model_name = models.CharField(max_length=40)
      change_type = models.BooleanField() # Insert-Update keep it False and Delete Keep it True
      record = models.IntegerField()

      class Meta:
         unique_together = ('institute','model_name', 'change_type','record',)

      def __str__(self):
         return self.institute


class Upload_File(models.Model):
       file_name=models.FileField(upload_to='images')


class Group_Info(models.Model):

       group_name = models.CharField(max_length=50,  unique=True)
       def __str__(self):
         return self.id

       def __unicode__(self):
         return unicode(self.group_name)


class Group_Details(models.Model):

       student = models.ForeignKey(User, db_index=True)

       group = models.ForeignKey(Group_Info, db_index=True)
       def __str__(self):
           return self.student

       def __unicode__(self):
           return unicode(self.student)

        
